# Melange template with Vite

Simple Melange starter project using Vite. Melange itself is built with esy, but Vite is installed and launched with npm.

## Usage

### Install dependencies

```bash
npm run setup
```

### Run a development server

```bash
npm run dev
```

### Generate a production build

```bash
npm run build
```
