import { defineConfig } from 'vite'
import melangePlugin from 'vite-plugin-melange'

export default defineConfig({
  plugins: [melangePlugin()],
  server: {
    watch: {
      awaitWriteFinish: {
        stabilityThreshold: 500,
        pollInterval: 20
      }
    }
  }
})
